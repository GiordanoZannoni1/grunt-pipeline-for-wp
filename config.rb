require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "src/css"
sass_dir = "src/sass"
images_dir = "src/images"
javascripts_dir = "src/js"
# fonts_dir = "fonts"
fonts_path = "acismom/fonts"
# http_fonts_path = "fonts"
additional_import_paths = "fonts/font-awesome"

output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

line_comments = false
color_output = false

preferred_syntax = :scss
