module.exports = function (grunt) {

	var config = {
			domain: '<%= pkg.settings.domain %>',
			staging: '<%= pkg.settings.staging %>',
			root: './',
	    app: 'src',
	    urlTest: '',
			dev: 'local',
	    urlDist: '<%= pkg.settings.urlDist %>',
			urlCustomLibs: 'js/libs',
			dist: '<%= pkg.settings.build %>'
	  };


    // Configuration
    grunt.initConfig({
        config: config,
        pkg: grunt.file.readJSON('package.json'),




        'http-server': {
            'dev': {
                // the server root directory
                root: './',
                // the server port
                // can also be written as a function, e.g.
                // port: function() { return 8282; }
                port: 2323,
                // the host ip address
                // If specified to, for example, "127.0.0.1" the server will
                // only be available on that ip.
                // Specify "0.0.0.0" to be available everywhere
                host: "localhost",
                //cache: false,
                showDir : true,
                autoIndex: true,
                // server default file extension
                ext: "html",
                // run in parallel with other tasks
                runInBackground: true,
                // specify a logger function. By default the requests are
                // sent to stdout.
                logFn: function(req, res, error) {

								},
                // Proxies all requests which can't be resolved locally to the given url
                // Note this this will disable 'showDir'
                //proxy: "http://someurl.com",

                /// Use 'https: true' for default module SSL configuration
                /// (default state is disabled)
                https: false,

                // Tell grunt task to open the browser
                openBrowser : true

                // customize url to serve specific pages
                //customPages: {
                //    "/readme": "README.md",
                //    "/readme.html": "README.html"
                //}
            }
         },

        watch: {
            /*gruntfile: {
                files: ['Gruntfile.js']
            },
            */
            html:{
                files: ['<%= config.app %>/**/*.html'],
                tasks: ['clean:dev_html','string-replace:dev_HTML']
            },
            php:{
                files: ['<%= config.app %>/*.php'],
                tasks: ['sync']
            },
						compass: {
								files: ['<%= config.app %>/sass/*.{scss,sass}'],
								tasks: ['compass:dev','autoprefixer:dev']
						},
            css: {
                files: ['<%= config.app %>/css/**/*'],
                tasks: ['sync']
            },
            js: {
                files: ['<%= config.app %>/js/{,*/}*.js'],
                tasks: ['sync','string-replace:dev_JS']
            },
            img:{
                files: ['<%= config.app %>/images/{,*/}*'],
                tasks: ['clean:dev_images','imagemin:dev']
            },
            fonts:{
                files: ['<%= config.app %>/fonts/{,*/}*'],
                tasks: ['sync']
            },
						options: {
							livereload: true
						}
        },


        sync: {
						main: {
								files: [{
									cwd: '<%= config.app %>',
									src: [
										'*.html',
										'*.htm',
										'*.php',
										'js/**/*',
										'css/**/*',
										//'!css/**/*.scss',
										//'!css/sass/**',
										'fonts/**',
										'images/*',
									],
									failOnError: true,
									dest: '<%= config.dev %>'
								}],
								//pretend: true, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
									verbose: true, // Display log messages when copying files
									updateAndDelete: false,
									ignoreInDest: '<%= config.dev %>/js/scriptloader.js'
								}
        },

        compass: {
            dev: {
                options: {
										sourcemap: true,
										specify: ['<%= config.app %>/sass/main.scss','<%= config.app %>/sass/header.scss'],
                    //sassDir: '<%= config.app %>/sass',
                    cssDir: '<%= config.dev %>/css',
										trace: true,
                    outputStyle: 'expanded'
                }
            },
            prod: {
                options: {
										sourcemap: true,
										specify: ['<%= config.app %>/sass/main.scss','<%= config.app %>/sass/header.scss'],
                    //sassDir: '<%= config.app %>/sass',
                    cssDir: '<%= config.dist %>/css',
                    outputStyle: 'compressed',
                    noLineComments: true
                }
            }
        },

        // Autoprefixer
        autoprefixer: {
						dev: {
								options: {
										browsers: ['last 3 versions', '> 1%', 'ie 8', 'ie 7'],
										map: true
								},
								files: [{
										'<%= config.dev %>/css/main.css' : '<%= config.dev %>/css/main.css'
									},{
										'<%= config.dev %>/css/header.css' : '<%= config.dev %>/css/header.css'
								}]
						},
            prod: {
                options: {
                    browsers: ['last 3 versions', '> 1%', 'ie 8', 'ie 7'],
										map: true
                },
                files: [{
                    '<%= config.dist %>/css/main.css' : '<%= config.dist %>/css/main.css'
									},{
										'<%= config.dist %>/css/header.css' : '<%= config.dist %>/css/header.css'
                }]
            }
        },

				cssmin: {
				  options: {
				    mergeIntoShorthands: false,
				    roundingPrecision: -1,
						sourceMap: true
				  },
				  target: {
				    files: [{
				      '<%= config.dist %>/css/main.min.css': ['<%= config.dist %>/css/main.css']
				    },{
							'<%= config.dist %>/css/header.min.css': ['<%= config.dist %>/css/header.css']
						}]
				  }
				},

        // Critical css
        critical: {
					inject: {
							options: {
									inline:true,
									base: '<%= config.dist %>',
									css: [
											'<%= config.dist %>/css/main.css'
									],
									width: 1440,
									height: 900,
									minify: true
							},
							files: '<%= templateName %>'
					},
					extract: {

							options: {
									inline: false,
									extract: true,
									base: '<%= config.dist %>',
									css: [
											'<%= config.dist %>/css/main.css'
									],
									width: 1440,
									height: 900,
									minify: true
							},
							files: '<%= templateName %>'

					},
					download: {

							options: {
									//inline: false,
									extract: false,
									base: '<%= config.dev %>',
									css: [
											'<%= config.dev %>/css/main.css'
									],
									width: 1440,
									height: 900,
									minify: true
							},
							src: '<%= config.dev %>/temporary-template/*.html',
							dest: '<%= config.dist %>css/'

					}

				},


        // Imagemin
        imagemin: {
           dev: {
              options: {
                optimizationLevel: 5
              },
              files: [{
                 expand: true,
                 cwd: '<%= config.app %>/images',
                 src: ['**/*.{png,jpg,gif,svg,ico}'],
                 dest: '<%= config.dev %>/images/'
              }]
           },
					 prod: {
              options: {
                optimizationLevel: 5
              },
              files: [{
                 expand: true,
                 cwd: '<%= config.app %>/images',
                 src: ['**/*.{png,jpg,gif,svg}'],
                 dest: '<%= config.dist %>/images/'
              }]
           }
        },

        uglify: {
						dev: {
								options: {
										compress: {
												drop_console: false
										},
										sourceMap: {
						          includeSources: true
						        },
										mangle: false
								},
								files: [{
										expand: true,
										cwd: '<%= config.app %>/js/',
										src: '**/*.js',
										dest: '<%= config.dev %>/js/'
								}]
						},
            prod: {
                options: {
                    compress: {
                        drop_console: true
                    },
										sourceMap: {
						          includeSources: true
						        },
										mangle: false
                },
                files: [{
										'<%= config.dist %>js/main.js': ['<%= config.dev %>/js/*.js','!<%= config.dev %>/js/scriptloader.js']
                }]
            }
        },

        // Replace
        'string-replace': {
						dev_HTML: {
							files: [{
								expand: true,
								cwd: '<%= config.app %>/',
								dest: '<%= config.dev %>/',
								src: ['*.html']
							}],
							options: {
								replacements: [{
									pattern: /@css_extension/g,
									replacement: ''
								},{
									pattern: /@@url/g,
									replacement: '../bower_components'
								}]
							}
						},


						dev_JS: {
							files: [{
								expand: true,
								cwd: '<%= config.app %>/js/',
								dest: '<%= config.dev %>/js/',
								src: ['scriptloader.js']
							}],
							options: {
								replacements: [{
									pattern: /@@urlmain/g,
									replacement: '<%= config.urlTest %>js'
								}, {
									pattern: /@@url/g,
									replacement: '../bower_components'
								}, {
									pattern: /@@version/g,
									replacement: '<%= Math.floor((Date.now() / 1000)) %>'
								}]
							}
						},


						prod_HTML: {
							files: [{
								expand: true,
								cwd: '<%= config.app %>/',
								dest: '<%= config.dist %>/templates/',
								src: ['*.html']
							}],
							options: {
								replacements: [{
										pattern: /@css_extension/g,
										replacement: '.min'
									},{
										pattern: /@@url/g,
										replacement: '<%= config.urlDist %>'
									}]
							}
						},


						prod_JS: {
							files: [{
								expand: true,
								cwd: '<%= config.app %>/js/',
								dest: '<%= config.dist %>/js/',
								src: ['scriptloader.js']
							}],
							options: {
								replacements: [{
									pattern: /@@urlmain/g,
									replacement: '<%= config.urlDist %>'
								},{
									pattern: /@@url.*[\/]/g,
									replacement: '<%= config.urlDist %>/libs/'
								}, {
									pattern: /@@version/g,
									replacement: '<%= Math.floor((Date.now() / 1000)) %>'
								}]
							}
						}


        },

				clean: {
					options: {
				    'force': true
				  },
					dev_html: {
						src: ['<%= config.dev %>/*.html']
					},
					dev_images:{
						src: ['<%= config.dev %>/images/']
					},
				  prod: {
				    src: ['<%= config.dist %>/']
				  }
				},

				bowercopy: {
					libs: {
	            options: {
	                destPrefix: '<%= config.dist %>/js/libs'
	            },
	            files: {
	                'jquery.min.js': 'jquery/dist/jquery.min.js',
	                'jquery-mobile-events.js': 'jquery-mobile-events/jquery-mobile-events.js',
									'Modernizr.js':'modernizr/src/Modernizr.js',
									'owl.carousel.min.js':'owl.carousel/dist/owl.carousel.min.js',
									'slideout.min.js':'slideout.js/dist/slideout.min.js',
									'jquery.sticky-kit.min.js':'sticky-kit/jquery.sticky-kit.min.js'
	            },
	        }
				},

				copy: {
						prod: {
					    files: [
					      {expand: true, cwd: '<%= config.app %>', src: ['fonts/**/*'], dest: '<%= config.dist %>'}
					    ]
					  }
					},

					http: {
				    download: {
				      options: {
				        url: 'http://admin:888@<%= templateUrl %>',
								timeout: 7000
				      },
				      dest: '<%= config.app %>/temporary-template/<%= downloadedTemplateName %>.html'
				    }
				  }

    });
    grunt.loadNpmTasks ('grunt-http-server');
    grunt.loadNpmTasks ('grunt-contrib-watch');
    grunt.loadNpmTasks ('grunt-sync');
    grunt.loadNpmTasks ('grunt-contrib-copy');
    grunt.loadNpmTasks ('grunt-autoprefixer');
    grunt.loadNpmTasks ('grunt-contrib-compass');
		grunt.loadNpmTasks ('grunt-contrib-cssmin');
		grunt.loadNpmTasks ('grunt-contrib-clean');
    grunt.loadNpmTasks ('grunt-critical');
		grunt.loadNpmTasks ('grunt-http');
    grunt.loadNpmTasks ('grunt-contrib-uglify');
		grunt.loadNpmTasks ('grunt-string-replace');
    grunt.loadNpmTasks ('grunt-contrib-imagemin');
		grunt.loadNpmTasks ('grunt-http');
		grunt.loadNpmTasks ('grunt-bowercopy');




    // Tasks


    grunt.registerTask('start', function(){
			grunt.file.mkdir(config.dev);
			grunt.task.run(['compass:dev', /*'autoprefixer:dev',*/ 'sync',/*'critical', 'criticalcss',*/ 'uglify:dev', 'string-replace:dev_HTML',/*'replace:dev_HTML',*/ 'string-replace:dev_JS', /*'replace:dev_JS',*//*'replace:version',*/'imagemin:dev','http-server:dev','watch']);
		});

    grunt.registerTask('server', [/*'sync:dev', */'string-replace:dev_HTML',/*'replace:dev_HTML',*/'http-server:dev','watch']);

		grunt.registerTask('build', ['copy:prod','compass:prod','autoprefixer:prod', 'cssmin','clean','bowercopy','uglify:prod','imagemin:prod',/*'critical:inject',*/'string-replace:prod_HTML','string-replace:prod_JS', 'criticalcss']);

		grunt.registerTask('main-css', ['compass:prod', 'autoprefixer:prod', 'cssmin']);

		grunt.registerTask('css', ['main-css', 'criticalcss']);

    grunt.registerTask('js_dev', ['uglify:dev', 'string-replace:dev_JS'/*, 'replace:version'*/]);

    grunt.registerTask('js_prod', ['uglify:prod', 'string-replace:prod_JS'/*,'string-replace:version'*/]);

    grunt.registerTask('images', ['imagemin']);

		grunt.registerTask('criticalcss', 'My "criticalcss" task.', function() {

			var mode = grunt.option('mode') || 'extract';
			var miofile = grunt.option('file') || '';

			//var mode = inject? 'inject' : 'extract';

			var pkg = grunt.file.readJSON('package.json');
			var templates = grunt.file.expand({filter: "isFile", cwd: "local/"},['*.html']);
			//grunt.log.warn(templates);

			var templatesCss = {};

			if( templates.indexOf(miofile + ".html") != -1){
				templates = [miofile];
			}
			//console.log(mode);
			if(mode == 'inject'){

				templates.map(function (file) {
						var name = file.replace(/\.[^/.]+$/, "");
						templatesCss[ pkg.settings.build + "templates/critical-" + name + ".html"] = pkg.settings.build + "templates/" + name + ".html";
						//grunt.log.warn(templatesCss);
						grunt.config.set('templateName', templatesCss);
				});

				//grunt.task.run('critical:inject');
				console.log("inserisco i css in linea...");
				grunt.task.run('critical:'+ mode);

			}else if(mode == 'extract'){

				templates.map(function (file) {
					var name = file.replace(/\.[^/.]+$/, "");
						templatesCss[ pkg.settings.build + "css/critical-" + name + ".min.css"] = pkg.settings.build + "templates/" + name + ".html";
						//grunt.log.warn(templatesCss);
						grunt.config.set('templateName', templatesCss);
				});

				console.log("estraggo i css...");
				grunt.task.run('critical:'+ mode);
			}else{
				grunt.log.warn("opzione non valida.");
			}

		});


		grunt.registerTask('download', 'download template', function(){
			var url = grunt.option('url') || '';
			var name = grunt.option('name') || '';
			//pkg = grunt.file.readJSON('package.json');
			grunt.config.set('templateUrl', url);
			grunt.log.writeln("downloading template...");
			grunt.config.set('downloadedTemplateName', name);
			grunt.task.run('http');
		});


};
