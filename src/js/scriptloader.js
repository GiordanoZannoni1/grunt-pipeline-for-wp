(function() {
    'use strict';

    //var nomeTema = '@@url';


    // Localstorage Check
    function skipCache(){
        var test = 'test';
        try {
            localStorage.setItem(test,test);
            localStorage.removeItem(test);
            return false;
        } catch(e) {
            return true;
        }
    }
    if(skipCache() == false){
        console.log('skipCache = false');
    } else {
        console.log('skipCache = true');
    }

    // Localstorage webFonts Check
    // var font_href = 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:900italic,900,700italic,700,600italic,400italic,600,400,300italic,300,200italic,200';
    // if(skipCache() == false && localStorage.webFonts){
    //     injectRawFontsStyle(localStorage.getItem('webFonts'));
    // } else {
    //     window.onload = function(){
    //         injectFontsStyle();
    //     }
    // }
    // function injectFontsStyle(){
    //     console.log('injectFontsStyle');
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('GET', font_href, true);
    //     xhr.onreadystatechange = function(){
    //         if (xhr.readyState === 4){
    //             injectRawFontsStyle(xhr.responseText);
    //             localStorage.setItem('webFonts', xhr.responseText);
    //         }
    //     }
    //     xhr.send();
    // }
    // function injectRawFontsStyle(text){
    //     console.log('injectRawFontsStyle');
    //     var style = document.createElement('style');
    //     style.innerHTML = text;
    //     document.getElementsByTagName('head')[0].appendChild(style);
    // }

    // BasketJs
    var version = '@@version';

    function requireScriptsMain() {
        return basket.require(
            {url: '@@urlmain/main.js', key:'mainJS', unique: '@@version', skipCache:true}
        )
    };

    function requireScriptsDelay2() {
        return basket.require(
            {url: '@@url/jquery-mobile-events/jquery-mobile-events.js', key:'jqueryUI', unique: '@@version', skipCache: skipCache()},
            {url: '@@url/modernizr/src/Modernizr.js', key:'modernizr', unique: '@@version', skipCache: skipCache()},
            {url: '@@url/owl.carousel/dist/owl.carousel.min.js', key:'owl', unique: '@@version', skipCache: skipCache()},
            {url: '@@url/sticky-kit/dist/sticky-kit.min.js', key:'sticky', unique: '@@version', skipCache: skipCache()},
						{url: '@@url/slideout.js/dist/slideout.min.js', key:'sticky', unique: '@@version', skipCache: skipCache()}
        ).then(requireScriptsMain);
    };
    //
    // function requireScriptsDelay() {
    //     return basket.require(
    //         {url: '@@urljs/libs/AnimOnScroll.js', key:'animonscroll', unique: version, skipCache: skipCache()}
    //     ).then(requireScriptsDelay2);
    // }

    basket.require(
        {url: '@@url/jquery/dist/jquery.min.js', key:'jquery', unique: '@@version', skipCache: skipCache()}
        ).then(requireScriptsDelay2);
}());
